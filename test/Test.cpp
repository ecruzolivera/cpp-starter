#include <Dummy.h>

#include <catch.hpp>

TEST_CASE("Test Dummy", "[algorithms,]")
{
  SECTION("Section 1")
  {
    CHECK(1 == 1);
  }
}

TEST_CASE("Test add", "[algorithms,arithmetics,]")
{
  SECTION("0+1=1")
  {
    CHECK(dummy::add(0, 1) == 1);
  }

  SECTION("1+1=2")
  {
    CHECK(dummy::add(1, 1) == 2);
  }
}

TEST_CASE("Test sub", "[algorithms,arithmetics,]")
{
  SECTION("0-1=-1")
  {
    CHECK(dummy::sub(0, 1) == -1);
  }

  SECTION("1-1=0")
  {
    CHECK(dummy::sub(1, 1) == 0);
  }
}