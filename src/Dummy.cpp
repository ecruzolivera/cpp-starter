#include "Dummy.h"

namespace dummy
{
auto add(int a, int b) -> int
{
  return a + b;
}

auto sub(int a, int b) -> int{
  return a-b;
}

auto div(int a, int b) -> int{
  return a/b;
}

}  // namespace dummy
