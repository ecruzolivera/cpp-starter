
#ifndef DUMMY_H
#define DUMMY_H

namespace dummy
{
/**
 * @brief
 * Addition function
 * @param a
 * @param b
 * @return a+b
 */
auto add(int a, int b) -> int;

/**
 * @brief
 * Sub function
 * @param a
 * @param b
 * @return a+b
 */
auto sub(int a, int b)-> int;



/**
 * @brief
 * Sub function
 * @param a
 * @param b
 * @return a+b
 */
auto div(int a, int b)-> int;



}  // namespace dummy

#endif  // DUMMY_H
