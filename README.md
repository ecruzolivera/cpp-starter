# Cpp Starter

CMake based project template heavily based in [https://github.com/lefticus/cpp_starter_project](https://github.com/lefticus/cpp_starter_project)

Example of generated [Doxygen](https://www.doxygen.nl/) docs [https://ecruzolivera.gitlab.io/cpp-starter](https://ecruzolivera.gitlab.io/cpp-starter)

## Features

- [x] CMake based release and debug build
- [x] Static Analyzers
  - [x] [CppCheck](http://cppcheck.sourceforge.net/)
  - [x] [clang-tidy](https://clang.llvm.org/extra/clang-tidy/)
  - [x] [scan-build (A.K.A Clang analyzer)](https://clang.llvm.org/extra/clang-tidy/)
- [x] Build Scrips for easy automation
- [x] Doxygen based documentation generation
- [x] Unit Tests support using [catch2](https://github.com/catchorg/Catch2)
- [x] GitLab CI/CD integration
- [ ] Examples subfolder
- [ ] Coverage Analysis
